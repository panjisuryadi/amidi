<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    // use HasFactory;
    
    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'desc', 'stock', 'lokasi', 'satuan'
    ];
}
