<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karyawan;
use App\Barang;
use App\Permintaan;
use Illuminate\Support\Facades\DB;

class AmidiController extends Controller
{
    public function index()
    {
        $karyawans      = Karyawan::latest()->paginate(10);
        $barangs        = Barang::latest()->paginate(10);
        $permintaans    = Permintaan::join('barangs', 'barangs.id', '=', 'permintaans.barang')
        ->join('karyawans', 'karyawans.nik', '=', 'permintaans.karyawan')
        ->select(
            'permintaans.id AS id', 
            'karyawans.name AS nama',
            'karyawans.department AS department', 
            'barangs.name AS barang', 
            'permintaans.qty AS qty', 
            'permintaans.keterangan AS keterangan', 
            'permintaans.tanggal AS request',
            'permintaans.created_at AS tanggal')
        ->latest('permintaans.id')
        ->get();
        return view('amidi', compact('karyawans', 'barangs', 'permintaans'));
    }

    public function store(Request $request){
        if(isset($request->stock)){
            $done = false;
            $error = "";
            $this->validate($request, [
                'nik'       => 'required',
                'tanggal'   => 'required',
                'barang'    => 'required',
                'qty'       => 'required'
            ]);

            $nik        = $request->nik;
            $tanggal    = $request->tanggal;
            $barang     = $request->barang;
            $qty        = $request->qty;
            $keterangan = $request->keterangan;
            $count      = count($barang);

            DB::beginTransaction();

            try {
                for ($i = 0; $i < $count; $i++) {
                    if (null !== ($barang[$i]) || null !== ($qty[$i] || null !== ($keterangan[$i]))) {
                        // CEK STOCK
                        $error  = "gagal di request barang ke";
                        $result = Barang::where('id', $barang[$i])
                                        ->where('stock', '>=', $qty[$i])->get();
                        if ($result->count() > 0) {
                            // INSERT
                            $permintaan = Permintaan::create([
                                'karyawan'      => $nik,
                                'barang'        => $barang[$i],
                                'qty'           => $qty[$i],
                                'keterangan'    => $keterangan[$i],
                                'status'        => 'A',
                                'tanggal'       => $tanggal
                            ]);

                            if (!$permintaan) {
                                throw new \Exception("Failed to create Permintaan");
                            }

                            // UPDATE
                            $barangUpdate = Barang::where('id', $barang[$i])->update(['stock' => \DB::raw("stock - $qty[$i]")]);

                            if ($barangUpdate <= 0) {
                                throw new \Exception("Failed to update Barang stock");
                            }
                        }
                    } else {
                        $done = false;
                    }
                }

                // If we reach here, all iterations were successful
                DB::commit();
                $done = true;
            } catch (\Exception $e) {
                // An error occurred, rollback the transaction
                DB::rollback();
                $error .= " " . $i + 1 . ": " . $e->getMessage();
            }

            if($done){
                return redirect()->route('amidi.index')->with(['success' => 'Success!']);
            }else{
                return redirect()->route('amidi.index')->with(['error' => 'Fail!'.$error]);
            }
        }
        elseif(isset($request->selectedValue)){
            $result = Karyawan::where('nik', '=', $request->selectedValue)->get();
        }
        elseif(isset($request->selectedBarang)){
            $result = Barang::where('id', '=', $request->selectedBarang)->get();
        }
        elseif(isset($request->barang)){
            $qty    = $request->qty;
            $result = Barang::where('id', $request->barang)
                            ->where('stock', '>=', $qty)->get();
            if ($result->count() > 0) {
                $result = 'Tersedia';
            } else {
                $result = 'Kurang';
            }
        }

        return response()->json(['message' => $result]);
    }
}
