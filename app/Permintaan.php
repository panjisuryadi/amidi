<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permintaan extends Model
{
    // use HasFactory;
    
    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'karyawan', 'barang', 'qty', 'keterangan', 'status', 'tanggal'
    ];
}
