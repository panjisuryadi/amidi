<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Requests Barang</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>

<div class="container mt-5">
    <h2>Requests</h2>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addItemModal">
        Add Request
    </button>

    <table class="table mt-3">
        <thead>
            <tr>
                <th>Name</th>
                <th>Department</th>
                <th>Barang</th>
                <th>QTY</th>
                <th>Keterangan</th>
                <th>Request For</th>
                <th>Created</th>
            </tr>
        </thead>
        <tbody>
            @foreach($permintaans as $permintaan)
                <tr>
                    <td>{{ $permintaan->nama }}</td>
                    <td>{{ $permintaan->department }}</td>
                    <td>{{ $permintaan->barang }}</td>
                    <td>{{ $permintaan->qty }}</td>
                    <td>{{ $permintaan->keterangan }}</td>
                    <td>{{ $permintaan->request }}</td>
                    <td>{{ $permintaan->tanggal }}</td>
                </tr>

                <div class="modal fade" id="editItemModal{{ $permintaan->id }}" tabindex="-1" role="dialog" aria-labelledby="editItemModalLabel{{ $permintaan->id }}" aria-hidden="true">
                </div>
            @endforeach
        </tbody>
    </table>

    <div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="addItemModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header bg-secondary">
                    <h5 class="modal-title" id="addItemModalLabel">Add Request</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('amidi.store') }}" id="myform" method="POST">
                        @csrf

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="requestTitle">NIK:</label>
                                <select name="nik" id="select_nik" onchange="tampil_karyawan();" class="form-control form-control-sm" required>
                                    <option value="">Pilih NIK</option>
                                    @foreach($karyawans as $karyawan)
                                    <option value="{{ $karyawan->nik }}">{{ $karyawan->nik }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="requestDescription">Nama:</label>
                                <input id="karyawan_name" type="text" class="form-control form-control-sm" name="name" readonly>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="requestDescription">Department:</label>
                                <input id="karyawan_department" type="text" class="form-control form-control-sm" name="department" readonly>
                            </div>
                            
                            <div class="form-group col-md-3">
                                <label for="requestDescription">Tanggal:</label>
                                <input type="date" name="tanggal" class="form-control form-control-sm" required>
                            </div>
                        </div>
                        
                        <br>
                        <button type="button" class="btn btn-success btn-sm" onclick="new_form();">Add Barang</button>
                        <br>
                        <br>

                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <input type="hidden" name="lanjut[]" id="lanjut[0]" required>
                                <label for="requestTitle">Barang:</label>
                                <select name="barang[0]" id="select_barang[0]" onchange="tampil_barang(0);" class="form-control form-control-sm" required>
                                    <option value="">Pilih Barang</option>
                                    @foreach($barangs as $barang)
                                    <option value="{{ $barang->id }}">{{ $barang->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="requestDescription">Lokasi:</label>
                                <input type="text" id="lokasi[0]" class="form-control form-control-sm" name="lokasi[0]" readonly>
                            </div>
                            <div class="form-group col-md-1">
                                <label for="requestDescription">Stock:</label>
                                <input type="number" id="stock[0]" class="form-control form-control-sm" name="stock[0]" readonly>
                            </div>
                            <div class="form-group col-md-1">
                                <label for="requestDescription">Qty:</label>
                                <input type="number" id="qty[0]" onkeyup="cek_qty(0);" class="form-control form-control-sm" name="qty[0]" required>
                            </div>
                            <div class="form-group col-md-1">
                                <label for="requestDescription">Satuan:</label>
                                <input type="text" id="satuan[0]" class="form-control form-control-sm" name="satuan[0]" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="requestDescription">Keterangan:</label>
                                <textarea name="keterangan[0]" id="keterangan[0]" class="form-control form-control-sm" id="" cols="30" rows="1"></textarea>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="requestDescription">Status:</label>
                                <input type="text" id="status[0]" class="form-control form-control-sm" name="status[0]" readonly>
                            </div>
                        </div>

                        <div id="additional_form"></div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>

    </div>

</div>

<!-- Bootstrap JS and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>
    @if(session()->has('success'))
        
        toastr.success('{{ session('success') }}', 'SUCCESS!'); 
   
    @elseif(session()->has('error'))

        toastr.error('{{ session('error') }}', 'FAIL!'); 
            
    @endif


    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $("#myform").submit(function (event) {
            let allLanjutInputs = $("input[name='lanjut[]']");
            
            let isAllLanjutSet = true;

            allLanjutInputs.each(function() {
                if (!$(this).val()) {
                    isAllLanjutSet = false;
                    return false;
                }
            });

            if (!isAllLanjutSet) {
                alert("Silahkan Isi QTY sesuai ketersedian Stock.");
                event.preventDefault();
            }
        });
    });
    
    function new_form(){
        let lokasiInputs = document.getElementsByName("lanjut[]");
        let numberOfInputs = lokasiInputs.length;
        newForm = `
            <div class="form-row">
                <div class="form-group col-md-2">
                    <input type="hidden" name="lanjut[]" id="lanjut[`+numberOfInputs+`]" required>
                    <select name="barang[`+numberOfInputs+`]" id="select_barang[`+numberOfInputs+`]" onchange="tampil_barang(`+numberOfInputs+`);" class="form-control form-control-sm" required>
                        <option value="">Pilih Barang</option>
                        @foreach($barangs as $barang)
                        <option value="{{ $barang->id }}">{{ $barang->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control form-control-sm" id="lokasi[`+numberOfInputs+`]" name="lokasi[`+numberOfInputs+`]" readonly>
                </div>
                <div class="form-group col-md-1">
                    <input type="number" class="form-control form-control-sm" id="stock[`+numberOfInputs+`]" name="stock[`+numberOfInputs+`]" readonly>
                </div>
                <div class="form-group col-md-1">
                    <input type="number" class="form-control form-control-sm" onkeyup="cek_qty(`+numberOfInputs+`);" id="qty[`+numberOfInputs+`]" name="qty[`+numberOfInputs+`]" required>
                </div>
                <div class="form-group col-md-1">
                    <input type="text" class="form-control form-control-sm" id="satuan[`+numberOfInputs+`]" name="satuan[`+numberOfInputs+`]" readonly>
                </div>
                <div class="form-group col-md-3">
                    <textarea name="keterangan[]" class="form-control form-control-sm" name="keterangan[`+numberOfInputs+`]" cols="30" rows="1"></textarea>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control form-control-sm" name="status[`+numberOfInputs+`]" id="status[`+numberOfInputs+`]" readonly>
                </div>
            </div>
        `;
        
        $("#additional_form").append(newForm);
    }

    function tampil_karyawan() {
        let selectedValue = $('#select_nik').val(); 
        $.ajax({
            url: "{{ route('amidi.store') }}",
            method: 'POST',
            data: {
                selectedValue: selectedValue
            },
            success: function (response) {
                $("#karyawan_name").val(response.message[0].name);
                $("#karyawan_department").val(response.message[0].department);
                console.log(response);
            },
            error: function (error) {
                console.error(error);
            }
        });
    }

    function tampil_barang(number) {
        let selectedBarang = $('#select_barang\\['+number+'\\]').val(); 
        $("#lanjut\\["+number+"\\]").val("");
        $("#qty\\["+number+"\\]").val("");
        console.log(number);
        console.log(selectedBarang);
        $.ajax({
            url: "{{ route('amidi.store') }}",
            method: 'POST',
            data: {
                selectedBarang: selectedBarang
            },
            success: function (response) {
                $("#lokasi\\["+number+"\\]").val(response.message[0].lokasi);
                $("#stock\\["+number+"\\]").val(response.message[0].stock);
                $("#satuan\\["+number+"\\]").val(response.message[0].satuan);
                $("#status\\["+number+"\\]").val("");
                console.log(response);
            },
            error: function (error) {
                console.error(error);
            }
        });
    }

    function cek_qty(number) {
        let qty = $('#qty\\['+number+'\\]').val(); 
        let barang = $('#select_barang\\['+number+'\\]').val(); 
        if(qty !== ''){
            $.ajax({
                url: "{{ route('amidi.store') }}",
                method: 'POST',
                data: {
                    qty: qty,
                    barang : barang
                },
                success: function (response) {
                    console.log(response);
                    $("#status\\["+number+"\\]").val(response.message);
                    if(response.message === 'Tersedia'){
                        $("#lanjut\\["+number+"\\]").val("true");
                    }else{
                        $("#lanjut\\["+number+"\\]").val("");
                        $("#lanjut\\["+number+"\\]").removeAttr("value");
                    }
                    console.log(response);
                },
                error: function (error) {
                    console.error(error);
                    $("#lanjut\\["+number+"\\]").removeAttr("value");
                }
            });
        }
    }
</script>
</body>
</html>
