<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Barang;
use Faker\Generator as Faker;

$factory->define(Barang::class, function (Faker $faker) {
    $nama   = [
        'A1-plastik',
        'A2-karton',
        'A3-amplop',
        'A4-kertas'
    ];

    return [
        'name' => $nama[random_int(0, 3)],
        'desc' => '',
        'satuan' => 'pak',
        'stock' => random_int(5, 45),
        'lokasi' => 'R'.random_int(1, 5)
    ];
});
