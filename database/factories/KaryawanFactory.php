<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Karyawan;
use Faker\Generator as Faker;

$factory->define(Karyawan::class, function (Faker $faker) {
    $department = [
        'it',
        'penjualan',
        'produksi',
        'lapangan',
        'manajemen',
    ];
    return [
        'name' => $faker->name,
        'nik' => $faker->numberBetween(10001, 10100),
        'department' => $department[random_int(0, 4)]
    ];
});
