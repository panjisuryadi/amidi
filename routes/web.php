<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\AmidiController;
// use app\Http\Controllers\AmidiController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('amidi', AmidiController::class);
// Route::post('amidi/tampil', [app\Http\Controller\AmidiController::class, 'tampil'])->name('amidi.tampil');
// Route::post('amidi/tampil', [AmidiController::class, 'tampil'])->name('amidi.tampil');
// Route::get('/your-route', [YourControllerName::class, 'yourMethodName']);
Route::get('/', function () {
    return view('welcome');
});
