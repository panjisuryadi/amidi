-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 08, 2024 at 04:19 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amidi`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `name`, `desc`, `stock`, `lokasi`, `satuan`, `created_at`, `updated_at`) VALUES
(5, 'A3-amplop', '', 178, 'R5', 'pak', '2024-03-08 02:23:06', '2024-03-08 08:18:16'),
(6, 'A2-karton', '', 120, 'R3', 'rim', '2024-03-08 02:23:06', '2024-03-08 08:18:16'),
(7, 'A4-kertas', '', 331, 'R4', 'rim', '2024-03-08 02:23:06', '2024-03-08 08:18:16'),
(9, 'A1-plastik', '', 167, 'R5', 'pak', '2024-03-08 02:23:06', '2024-03-08 08:10:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `karyawans`
--

CREATE TABLE `karyawans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nik` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `karyawans`
--

INSERT INTO `karyawans` (`id`, `nik`, `name`, `department`, `created_at`, `updated_at`) VALUES
(1, 10026, 'Sallie Muller', 'produksi', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(2, 10096, 'Caden Huels', 'it', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(3, 10091, 'Prof. Irma Berge I', 'it', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(4, 10054, 'Mrs. Kaylee Steuber Jr.', 'manajemen', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(5, 10046, 'Dr. Casimir Hauck', 'penjualan', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(6, 10100, 'Prof. Dudley Bergstrom', 'it', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(7, 10017, 'Dr. Forest Hammes', 'penjualan', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(8, 10062, 'Dr. Rhiannon Streich Jr.', 'lapangan', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(9, 10087, 'Miss Noemie Nolan I', 'produksi', '2024-03-08 02:23:00', '2024-03-08 02:23:00'),
(10, 10035, 'Audie Bartoletti', 'lapangan', '2024-03-08 02:23:00', '2024-03-08 02:23:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2024_03_08_091345_create_karyawans_table', 1),
(5, '2024_03_08_091355_create_barangs_table', 1),
(6, '2024_03_08_091406_create_permintaans_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permintaans`
--

CREATE TABLE `permintaans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `karyawan` int(11) NOT NULL,
  `barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` char(255) NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permintaans`
--

INSERT INTO `permintaans` (`id`, `karyawan`, `barang`, `qty`, `keterangan`, `status`, `tanggal`, `created_at`, `updated_at`) VALUES
(23, 10026, 5, 10, 'test 1', 'A', '2024-03-08', '2024-03-08 08:16:02', '2024-03-08 08:16:02'),
(24, 10096, 5, 44, 'jadi 200 sisa', 'A', '2024-03-09', '2024-03-08 08:16:32', '2024-03-08 08:16:32'),
(25, 10096, 6, 15, NULL, 'A', '2024-03-09', '2024-03-08 08:16:32', '2024-03-08 08:16:32'),
(26, 10091, 5, 15, NULL, 'A', '2024-03-08', '2024-03-08 08:17:21', '2024-03-08 08:17:21'),
(27, 10091, 6, 25, NULL, 'A', '2024-03-08', '2024-03-08 08:17:21', '2024-03-08 08:17:21'),
(28, 10091, 7, 40, 'sudah cukup', 'A', '2024-03-08', '2024-03-08 08:17:21', '2024-03-08 08:17:21'),
(29, 10054, 5, 7, 'test 2', 'A', '2024-03-08', '2024-03-08 08:18:16', '2024-03-08 08:18:16'),
(30, 10054, 7, 10, NULL, 'A', '2024-03-08', '2024-03-08 08:18:16', '2024-03-08 08:18:16'),
(31, 10054, 6, 10, NULL, 'A', '2024-03-08', '2024-03-08 08:18:16', '2024-03-08 08:18:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawans`
--
ALTER TABLE `karyawans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permintaans`
--
ALTER TABLE `permintaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `karyawans`
--
ALTER TABLE `karyawans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permintaans`
--
ALTER TABLE `permintaans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
